<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class DocumentoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //http://www.autobacks.pe/
    }

    public function ObtenerDocumentoElectronico(Request $request)
    {
        try {

            $_CodTipoDocumento = $request->codTipoDocumento;
            $_FechaEmision =  $request->fechaEmision;
            $_Serie =  $request->serie;
            $_Numero =  $request->numero;
            $_Total =  $request->total;

            $result = DB::select(
                'call ObtenerDocumentoElectronicoParaConsultaWeb(?,?,?,?,?)', array($_FechaEmision, $_Serie,$_Numero, $_Total, $_CodTipoDocumento)
            );

            if(empty($result)){
                $res['success'] = false;
            }else{
                $res['success'] = true;
            }

            $res['data'] = $result ;
            return response($res, 200);
        } catch (\Illuminate\Database\QueryException $ex) {
            $res['success'] = false;
            $res['message'] = $ex->getMessage();
            return response($res, 200);
        }
    }
}

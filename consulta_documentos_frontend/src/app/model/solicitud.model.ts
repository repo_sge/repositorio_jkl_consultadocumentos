export interface Solicitud {
  fechaEmision: String;
  serie: string;
  numero: string;
  total: number;
  codTipoDocumento: number;
}

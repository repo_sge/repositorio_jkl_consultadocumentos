import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppComponent } from './app.component';
import { registerLocaleData } from '@angular/common';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import localePE from '@angular/common/locales/es-PE';

import { NgxSpinnerModule } from 'ngx-spinner';
import { ConsultaService } from './services/consulta.service';
import { HttpClientModule } from '@angular/common/http';
import * as moment from 'moment';

import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

moment.locale('es-PE');

registerLocaleData(localePE, 'es-PE');

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    ClarityModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxSpinnerModule,
    SweetAlert2Module.forRoot()
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'es-PE' }, ConsultaService],
  bootstrap: [AppComponent],
})
export class AppModule {}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Solicitud } from './model/solicitud.model';

import { NgxSpinnerService } from 'ngx-spinner';
import { ConsultaService } from './services/consulta.service';

import * as moment from 'moment';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  found = false;
  documento: any;
  busqFrm: FormGroup;

  @ViewChild('swalAlert') private swalAlert: SwalComponent;

  constructor(
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private service: ConsultaService,
  ) {}

  ngOnInit(): void {
    this.busqFrm = this.fb.group({
      tipoDocumento: ['', Validators.compose([Validators.required])],
      serie: [
        '',
        Validators.compose([
          Validators.required,
          Validators.maxLength(3),
          Validators.minLength(3),
        ]),
      ],
      numero: [
        '',
        Validators.compose([
          Validators.required,
          Validators.maxLength(8),
          Validators.minLength(8),
        ]),
      ],
      fechaEmision: ['', Validators.compose([Validators.required])],
      total: ['', Validators.compose([Validators.required, Validators.min(0)])],
    });
  }

  get tipoDocumento() {
    return this.busqFrm.get('tipoDocumento');
  }

  get serie() {
    return this.busqFrm.get('serie');
  }

  get numero() {
    return this.busqFrm.get('numero');
  }

  get fechaEmision() {
    return this.busqFrm.get('fechaEmision');
  }

  get total() {
    return this.busqFrm.get('total');
  }

  consultarDocumento(): void {
    this.spinner.show();
    const dateWrapper = moment(this.busqFrm.value.fechaEmision, 'DD/MM/YYYY');

    if (this.busqFrm.valid) {
      const solicitud: Solicitud = {
        codTipoDocumento: this.busqFrm.value.tipoDocumento,
        fechaEmision: dateWrapper.format('YYYY-MM-DD'),
        serie: this.busqFrm.value.serie,
        numero: this.busqFrm.value.numero,
        total: this.busqFrm.value.total,
      };

      this.service.obtenerDocumentoElectronico(solicitud).subscribe(result => {
        this.spinner.hide();
        if (result) {
          if (result.success) {
            this.found = true;
            this.documento = result.data[0];
          } else {
            this.found = false;
            // Cambiar por un SweetAlert
            this.swalAlert.show();
          }
        }
      });
    }
  }

  limpiar() {
    this.busqFrm.reset();
    this.found = false;
    this.documento = null;
  }

  showPdf() {
    const linkSource = 'data:application/pdf;base64,' + this.documento.TramaPdf;
    const downloadLink = document.createElement('a');
    const fileName = this.documento.Documento + '.pdf';

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }

  showXML() {
    const linkSource = 'data:application/xml;base64,' + this.documento.TramaXml;
    const downloadLink = document.createElement('a');
    const fileName = this.documento.Documento + '.xml';

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }
}

import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Solicitud } from '../model/solicitud.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ConsultaService {
  baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) {}

  obtenerDocumentoElectronico(solicitud: Solicitud): Observable<any> {
    return this.http.post(this.baseUrl + 'getDocumento', solicitud);
  }
}
